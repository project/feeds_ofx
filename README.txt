OFX parser for Feeds

Usage:
  Install with Feeds and configure as a parser on your feed importer settings.

Credits:
  Feeds OFX module: Michael Stenta (drupal.org/user/581414)